import 'dart:html';

import 'package:htmlcomp/src/validators.dart';

class _ItemUrlPolicy implements UriPolicy {
  RegExp regex = RegExp(r'(?:https://)?.*');
  @override
  bool allowsUri(String uri) {
    return regex.hasMatch(uri);
  }
}

class GcxHtmlValidator {
    ButtonElement btn = ButtonElement();
    NodeValidatorBuilder validator = NodeValidatorBuilder()
  ..allowTextElements()
  ..allowTemplating()
  ..allowElement('button', attributes: ['onclick'])
  ..allowElement('input', attributes: ['onclick'])
  ..allowHtml5(uriPolicy: _ItemUrlPolicy())
  ..allowNavigation(_ItemUrlPolicy())
  ..allowImages(_ItemUrlPolicy())
  ..allowSvg();
  GodNodeValidator _vali = GodNodeValidator();
  GcxHtmlValidator();
  NodeValidatorBuilder get v => validator;

  //vali accept everything
  GodNodeValidator get vali => this._vali;
  set vali(GodNodeValidator vali){
  this._vali = vali;
  }
}
