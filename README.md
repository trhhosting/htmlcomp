#HTMLCOMP

```
import 'package:htmlcomp/htmlcomp.dart';
import 'dart:html';
void main() {
  var gcxval = GcxHtmlValidator();
  String url = 'https://google.com';
  String img = 'https://i.ytimg.com/vi/iNRQB5309yo/maxresdefault.jpg';
  DivElement div = DivElement();
  String rightTextHtml = '''
    <div class="trand-right-single d-flex">
      <div class="trand-right-img">
        <img height="120px" width="120px" src="${img}" alt="">
      </div>
      <div class="trand-right-cap">
        <span class="color3">Test</span>
        <h4>
          <a href="${url}" target="_blank">
            Color blind people
          </a>
        </h4>
      </div>
    </div>
    ''';
    div.appendHtml(rightTextHtml,
      validator: gcxval.validator
      );

}
```
